<img align = "center" src="/Assets/VacciLog.jpg" alt="VacciLog-Logo" title="VacciLog Logo">

<!-- <h1> Vaccination Logger </h1> -->
<h1> <strong>Vacci<span>Log</span></strong> </h1>

Vaccination logs can be registered in a blockchain, this will help to provide a secure and hassle-free vaccination with good detail traceability. The Personal details can be stored in a blockchain which will provide more secure and tamper-proof data storing and the details of vaccination can be done through the transaction. The relevant data can be retrieved through a Unique ID(Aadhar or any such IDs).

This will also provide flexibility to take the vaccine shot from different vaccination centre. The data and personal details will be secure and tamper-proof, and the vaccination relevant data can be traced without much time consumption.
This will be helpful in vaccination like COVID-19, Polio, TTP, etc.

<h2> Repo directory </h2>
    
    Assets
          ↳ Suporting Files
    Blockchain Chain Data
          ↳ config
            ↳ vaccilog.json [genesis file]
          ↳ data
            ↳ geth
            ↳ keystore
          ↳ New Account details
          ↳ backup
    Demo
    Ropsten Network Details
          ↳ Readme.md
          ↳ Txn Data Spreadsheet
          ↳ Ropsten Network Details.txt
    Tasks
          ↳ Task 01
          ↳ Task 02
    Vacci-Log
          ↳ Main Project
            ↳ Contracts
            ↳ Test files
            ↳ Other Supporting files
            ↳ Readme.md  [instruction to setup node and run the project]
    Documentation_Vacci-Log.pdf
    Project Proposal Document.pdf

<strong>
NB: Read Readme.md in respective folders before executing the code.
</strong>

## Files Included

1. [x] Web application (code commented)
2. [x] Blockchain Chain Data
3. [x] Smart Contract (code commented)
4. [x] Test file (code commented)
5. [x] Demo video
6. [x] Contract address and link to Etherscan
7. [x] Readme.md files
8. [x] Documentations
