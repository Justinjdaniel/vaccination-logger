<img align = "center" src="/Assets/VacciLog.jpg" alt="VacciLog-Logo" title="VacciLog Logo">

<h1> <strong>Vacci<span>Log</span></strong> </h1>

# How to setup the project

### 1. Run a node with **networkid: 5777** and **port: 7545**.

### 2. Clone this repository.

### 3. Go to the Vacci-Log directory.

    cd Vacci-log

### 4. Install all node pakages required.

    npm install

### 5. Compile and Deploy the contract.

    truffle compile
    truffle migrate

### 6. Start the project.

    npm start

### 7. Go to **localhost:3000**

    http://localhost:3000/


## Demo Video

<img src="VacciLog Demo.mp4" title="VacciLog Demo">

## Workflow
<img src="/Assets/Workflow.jpg" title="Workflow">

## Documentation
<object data="Documentation _ Vacci-Log.pdf" type="application/pdf" width="700px" height="700px">
    <embed src="Documentation _ Vacci-Log.pdf">
        <p>Please download the PDF to view it: <a href="Documentation _ Vacci-Log.pdf">Download Documentation</a>.</p>
    </embed>
</object>