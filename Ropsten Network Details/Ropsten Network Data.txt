Compiling your contracts...
===========================
> Compiling .\contracts\Migrations.sol
> Compiling .\contracts\Vaccine.sol
> Artifacts written to D:\Workspace\KBA\Project\vaccination-logger\Vacci-Log\build\contracts
> Compiled successfully using:
   - solc: 0.7.0+commit.9e61f92b.Emscripten.clang



Starting migrations...
======================
> Network name:    'ropsten'
> Network id:      3
> Block gas limit: 8000000 (0x7a1200)


1_initial_migration.js
======================

   Deploying 'Migrations'
   ----------------------
   > transaction hash:    0x61c8d8b84a42d8f9aaf2ad22f6bc01204ad82caac3d0e288b2b6e0a73376a275
   > Blocks: 1            Seconds: 25
   > contract address:    0x960bA11AB0f9569BBF928e6A5e7b2b3Fb0858988
   > block number:        9356575
   > block timestamp:     1609252357
   > account:             0xC3Ad456605d95804F57c0987D3B9d342EFb18675
   > balance:             9.94583427648070405
   > gas used:            186951 (0x2da47)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00373902 ETH

   Pausing for 1 confirmations...
   ------------------------------
   > confirmation number: 1 (block: 9356576)

   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.00373902 ETH


1607312465_vaccine.js
=====================

   Deploying 'Vaccine'
   -------------------
   > transaction hash:    0x15bea8dc3a9cf58a3781fbf86f04bed968d2c63721a3d9b3cb46f439486f549f
   > Blocks: 1            Seconds: 25
   > contract address:    0x21997Da46B90fE48475ceB3A36e255c498D4E653
   > block number:        9356579
   > block timestamp:     1609252477
   > account:             0xC3Ad456605d95804F57c0987D3B9d342EFb18675
   > balance:             9.93322625648070405
   > gas used:            588066 (0x8f922)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.01176132 ETH

   Pausing for 1 confirmations...
   ------------------------------
   > confirmation number: 1 (block: 9356582)

   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01176132 ETH


Summary
=======
> Total deployments:   2
> Final cost:          0.01550034 ETH