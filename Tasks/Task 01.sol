// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.7.0;

contract sudentDetails {
  // P1, P2, P3 represents the Phase {One, Two, Three}
  enum Status {
    P1P2P3_NotSubmitted,
    P1_Submitted_P2P3_NotSubmitted,
    P1P2_Submitted_P3_NotSubmitted,
    P1P2P3_Submitted
  }
    
    struct Student {
      uint studentId;
      string studentName;
      string submissionStatus;
      string gitLabUrl;
      string projectName;
      string projectDescription;
      Status status;
    }
  
  mapping(uint => Student) students;
  
  // adding student details 
  function addStudentDetails(uint _studentId, string memory _studentName, string memory _submissionStatus, string memory _GitLabUrl, string memory _projectName, string memory _description, Status _status) public{
    addOrEditStudent(_studentId, _studentName, _submissionStatus, _GitLabUrl, _projectName, _description, _status);
  }
  
  // changing students deatils
  // the funtion used inside this is same as that of adding deatils
  function editStudentDetails(uint _studentId, string memory _studentName, string memory _submissionStatus, string memory _GitLabUrl, string memory _projectName, string memory _description, Status _status) public{
    addOrEditStudent(_studentId, _studentName, _submissionStatus, _GitLabUrl, _projectName, _description, _status);
  }
  
  // private function for storing Student data   
  function addOrEditStudent(uint _studentId, string memory _studentName, string memory _submissionStatus, string memory _GitLabUrl, string memory _projectName, string memory _description, Status _status) private{
    students[_studentId].studentId = _studentId;
    students[_studentId].studentName = _studentName;
    students[_studentId].submissionStatus = _submissionStatus;
    students[_studentId].gitLabUrl = _GitLabUrl;
    students[_studentId].projectName = _projectName;
    students[_studentId].projectDescription = _description;
    students[_studentId].status = _status;
  }
  
  // viewing student details
  function viewStudentDeatils(uint _studentId) view external returns (uint, string memory, string memory, string memory, string memory, string memory, Status){
    return (
      students[_studentId].studentId,
      students[_studentId].studentName,
      students[_studentId].submissionStatus,
      students[_studentId].gitLabUrl,
      students[_studentId].projectName,
      students[_studentId].projectDescription,
      students[_studentId].status
      );
  }
}