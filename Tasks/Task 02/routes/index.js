var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Club Rewards', address: contractAddress });
});

router.post('/enrollStudent', function (req, res, next) {
  data = req.body;
  console.log(data);
  MyContract.methods.enrollStudent(data.id, data.studentName)
    .send({ from: data.accountAddress })
    .then((txn) => {
      res.send(txn);
    })
});

router.post('/startAClub', function (req, res, next) {
  data = req.body;
  console.log(data);
  MyContract.methods.startAClub(data.clubId, data.clubName, data.inchargeAddress)
    .send({ from: data.accountAddress, gas: 800000 })
    .then((txn) => {
      res.send(txn);
    })
});

router.post('/enrollStudentInClub', function (req, res, next) {
  data = req.body;
  console.log(data);
  MyContract.methods.enrollStudentInClub(data.id, data.clubId, data.inchargeAddress)
    .send({ from: data.inchargeAddress })
    .then((txn) => {
      res.send(txn);
    })
});

router.post('/reward', function (req, res, next) {
  data = req.body;
  console.log(data);
  MyContract.methods.reward(data.studentAddress, data.amount)
    .send({ from: data.accountAddress })
    .then((txn) => {
      res.send(txn);
    })
});

router.post('/balanceOf', function (req, res, next) {
  data = req.body
  MyContract.methods.balanceOf(data.accountAddress)
    .call({ from: data.accountAddress })
    .then((result) => {
      res.send(result);
    })
});

module.exports = router;